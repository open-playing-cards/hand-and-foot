# Hand & Foot

## Setting Up the Game

1. You need 4 or 6 players
1. Include one full deck of playing cards, including Jokers, per player (e.g. if 4 people are playing, you need 4 full decks of cards)
1. Divide into 2 equal teams sitting in alternating positions (i.e. Team A Person 1, Team B Person 1, Team A Person 2, Team B Person 2, etc.)
1. Thoroughly shuffle all of the cards & divide them into one or more face-down piles – these will be the **draw piles**
1. Randomly decide who will be the starting player for the first round; on subsequent rounds, the player to the starting player’s left will become the new starting player

## The Cards

### Bad Cards

1. 3s are considered bad cards as they cannot be made into books (they are simply dead weight in your hand)
1. Black 3s are 50 points each
1. Red 3s are 300 points each

### Wild Cards

1. 2s and Jokers are wild cards
1. 2s are worth 20 points each
1. Jokers are worth 50 points each

### Regular Cards

1. Regular cards are those that can be made into books
1. 4s, 5s, 6s, and 7s are worth 5 points each
1. 8s, 9s, 10s, Jacks, Queens, and Kings are worth 10 points each
1. Aces are worth 20 points each

## Starting a Round

1. The starting player tries to take exactly 22 cards off the top of the draw pile (there can be multiple draw piles); if they managed to grab exactly 22 cards, they earn their team a bonus of 100 points
1. The player to the starting player’s left follows this same action, and the players continue doing this clockwise around the table until each player has done so (as with the starting player, each player that manages to get exactly 22 cards earns their team a bonus of 100 points)
1. Each player now ensures that they have exactly 22 cards and divides them, however they wish, without looking at the cards, into two piles of 11 cards each
1. Each player then decides, again without looking at any of the cards, which pile will be their **Hand** (the first set of cards they will play through) and which will be their **Foot** (the second set of cards they will play through)
1. Once everybody has their 22 cards divided up and has chosen their Hand & their Foot, the top card of one of the face-down decks is placed face up in the middle of the table – this is the discard pile
1. Each player can now look at the cards in their Hand
1. Play begins with the starting player

## General Game Play

1. On a player’s turn, they have the choice of either [Picking Up the Pile](#picking-up-the-pile) or drawing two face-down cards from any of the draw piles
1. If the player’s team has not yet laid down, they will need to meet the requirements for [Laying Down](#laying-down) in order to play any cards out of their hand
1. If the player’s team has already laid down, then the player can add cards to any of the books on their team’s board or start new books (see the rules for [Creating, Adding to, and Closing Books](#creating-adding-to-and-closing-books))
1. Once the player has played out any cards they are able to, they will discard one card (except in the case that the player is able to play out all of their cards; in that case, they do not need to discard a card)
1. If the player was able to get rid of all of the cards in their Hand (either by playing & discarding or playing them all out) they move on to the 11 cards in their Foot
1. If the player is already in their Foot and wishes to play out all their cards, they must meet the requirements for [Going Out](#going-out)
1. Play then proceeds clockwise

## Picking Up the Pile

1. If the top card of the discard pile is a 3 (red or black), the pile cannot be picked up
1. If the top card of the discard pile is not a 3, and the player makes the choice to pick up the pile, the player does not draw any cards from the draw piles this turn
1. In order to pick up the pile, the player must be able to immediately use (i.e. put down on their team’s board) the top card of the discard pile, pairing it only with cards from their own hand and/or their team’s board
1. Once the player has used the top card, they then pick up all the rest of the cards in the discard pile
1. They can then play out any of the cards they just picked up
1. They still end their turn by discarding one card

## Laying Down

1. Laying down cards consists of [creating & adding to books](#creating-adding-to-and-closing-books)
1. In order to lay down cards for the first time & create your team’s board, you must meet the following criteria based on your team’s current score (see [Scoring](#scoring))

   | Team’s Score   | Points Needed to First Lay Down |
   | -------------- | ------------------------------- |
   | 0 to 2,995     | 50                              |
   | 3,000 to 4,995 | 90                              |
   | 5,000 or more  | 120                             |

## Creating, Adding to, and Closing Books

1. A book consists of 3 or more cards that are all the same number
1. A **natural** (or **clean**) book consists only of regular cards (e.g. seven 8s)
1. An **unnatural** (or **dirty**) book consists of a mix of regular cards and wild cards (e.g. five 8s, one Joker, and one 2)
1. An unnatural book cannot have more wild cards than regular cards (until the book is closed, then it can have any number of wild cards)
1. A book is **closed** when it contains 7 cards; a natural book cannot be added to once it is closed; an unnatural book can have more cards added to it even after it is closed
1. Closed natural books are worth 500 points each; closed unnatural books are worth 300 points each
1. Your team may only have one open book per number at a time (e.g. if you already have an unnatural book of Aces, containing 5 Aces and 1 Joker, you may not start a new book of Aces until you first add one Ace or wild card to the already open book of Aces, thus closing it)
1. Once your team has a closed book of some number, you are free to start a new book for that number (e.g. if your team has a closed natural book of 4s you may start a new book of 4s)

## Going Out

1. Going out means that a player is in their Foot and they are able to get rid of all of the cards in their hand
1. In order for a player to be able to go out, their team must have at least two closed natural books
1. Once a player goes out, the round is immediately over

## Scoring

### Card Values

1. 4s, 5s, 6s, and 7s are worth 5 points each
1. 8s, 9s, 10s, Jacks, Queens, and Kings are worth 10 points each
1. Aces and 2s are worth 20 points each
1. Jokers and black 3s are worth 50 points each
1. Red 3s are worth 300 points each
1. Remember that 3s (red or black) cannot be made into books and therefore always count against you!

### End of Round Tally

1. Any cards left in your Hand and/or Foot count against you
1. Any natural books that your team has completed are worth 500 points each
1. Any unnatural books that your team has completed are worth 300 points each
1. Add up the point values of all of the cards from your team’s board, including the cards that were part of completed books

### Scoring Example

The round has ended and you still have a few cards in your Hand and your entire Foot is still sitting on the table. Your team has three natural books completed and five unnatural books completed. Your team sores 1,500 points for the three natural books (3 × 500) and 1,500 points for the five unnatural books (5 × 300). Let’s say your team also has 1,050 points in cards. In your Hand you have an ace (-20), an eight (-10), and a four (-5) for a total of -35 points, and your Foot has one red three (-300), one black three (-50), two Jokers (-50 each), one ace (-20), two kings (-10 each), two tens (-10 each), one six (-5), and one five (-5), totalling -520 points! So, in the end, your team would have a final score for the round of 1,500 + 1,500 + 1,050 - 520 = 3,530 points.

## End of Game

1. The game ends when a team’s score goes over 15,000 points
1. The team with the most points wins
